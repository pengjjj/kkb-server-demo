package com.kkb.hk.dao;

import com.kkb.hk.entity.HkBanner;
import com.kkb.hk.vo.request.banner.HkBannerRequest;
import com.kkb.hk.vo.response.banner.HkBannerResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @className HkBannerDao
 * @description:  表(HkBanner)表数据库访问层
 * @author Allen
 * @date 2021/12/16 15:49
 */
@Mapper
public interface HkBannerDao {

    /**
     * @description:查询banner列表
     * @param: [hkBannerRequest]
     * @return: java.util.List<com.kkb.hk.vo.response.banner.HkBannerResponse>
     * @author Allen
     * @date: 2021/12/16 16:25
     */
    List<HkBannerResponse> qryList(HkBannerRequest hkBannerRequest);

    /**
     * @description:查询banner列表分页查询
     * @param: [hkBannerRequest]
     * @return: java.util.List<com.kkb.hk.vo.response.banner.HkBannerResponse>
     * @author Allen
     * @date: 2021/12/16 16:27
     */
    List<HkBannerResponse> qryListByPage(HkBannerRequest hkBannerRequest);

    /**
     * @description:新增banner列表元素
     * @param: [hkBanner]
     * @return: int
     * @author: PengJun
     * @date 2021/12/27 14:57
     */
    int addList(HkBanner hkBanner);

    /**
     * @description:通过用户ID删除banner列表数据
     * @param: [bannerId]
     * @return: int
     * @author: PengJun
     * @date 2021/12/27 14:47
     */
    int delList(Integer bannerId);

    /**
     * @description:修改banner列表元素
     * @param: [hkBanner]
     * @return: int
     * @author: PengJun
     * @date 2021/12/27 14:47
     */
     int editList(HkBanner hkBanner);




}

